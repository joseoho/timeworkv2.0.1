
<?
// require 'vendor/autoload.php';
// use Intervention\Image\ImageManager;
?>
<!DOCTYPE html>
<html lang="es">
    <head>
<style>



</style>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Capturar Webcam con HTML5 y Javascript">
    <meta name="author" content="Raúl Prieto Fernández">
    <title>Capturar Webcam con HTML5 y Javascript</title>
    <link href="/images/style.css" rel="stylesheet" type="text/css">
    </head>
<body onload="">

<!-- Cabecera -->
<h1>Registre su ingreso, Laboratorio Clinico BILOBA</h1>


<input type="fecha" name="fecha" id="fecha" class="form-control" value="{{$fecha}}" readonly="true" aling="center">								
<!-- se moficio este archivo linea 29 -->

<!-- Mensaje de error -->
<span name="errorMsg"></span>

<!-- Cargar Frame de TV y cargar el video de la webcam -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
										@if (session()->has('notificacion'))
                        					<div class="alert alert-success" role="alert">Registrado</div>
                        					{{session('notificacion')}}
                    					@endif
									
										 @if (session()->has('noexiste'))
                         					 <div class="alert alert-danger" role="alert">Cedula no Registrada, Verifique</div>
											  {{session('noexite')}}
                      					@endif 
<form action="{{ route('IngresoEmpleado.store') }}" method="POST" file="true" name="IngresoEmpleado" enctype="multipart/form-data">
@csrf
<div class="row">
	
		<div class="col-12 grid-margin">
			<div class="card">
				<div class="row">
				
					<div class="col-md-6">
						
						<div class="card-body">
							
								<!-- <div class="form-group"> -->
								<div class="form-group">
									
									<label for="empleado_id"><strong>Cedula</strong></label>
									<input type="number" name="empleado_id" id="empleado_id" class="form-control" placeholder="Ingrese numero cedula"  tabindex="0" autofocus required>
									<input class="form-control" type= "hidden" name="data_url" id="data_url"  accept ="" class="form-control"></input>
								</div>
								
								<div id="video_wrap">
    								<div id="video_overlays">
										<!-- <img src="/images/tv.jpg"></img> -->
									</div> 
    									{{-- <video id="video_frame" playsinline autoplay></video> --}}
										<video id="video_frame" preload="true" playsinline autoplay muted></video>
										
								</div>
									<!-- Capturamos la imagen a través de la API web y lo mostramos en el canvas -->
									<div class="button_controller">
										<!-- Botón para capturar -->
										<button id="snap_frame">Capturar Imagen</button>
										<br/><br/>
										<!-- Imagen de la Webcam -->
										<canvas id="canvas_frame"></canvas>
										<input type="hidden" name="imagen" value="" id="imagen"/>
										
										
									</div>

									
										
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
										
											<button class="btn btn-success float-right mt-4 ml-2 " id="agregar" onclick="" tabindex="1" >Registar</button> 
		
</form>	
</div>
	
	@section('scripts')
	
	
	@endsection
<!-- Cargamos el Javascript -->
<script src="/webcam.js" async></script>
<script src="/teclaenter.js" ></script>


</body>
</html>