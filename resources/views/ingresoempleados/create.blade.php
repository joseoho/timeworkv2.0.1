@extends('layouts.templates')
@section('title', 'Registrar su Ingreso y/o Retiroo')
@section('styles')
  <link rel="stylesheet" href="{{ asset('assets/css/toastr.min.css') }}">
@endsection
@section('content')
////probando repo
	<div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
              Control de Asistencia
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('IngresoEmpleado.index') }}">Registro de Ingreso y/o Salida</a></li>
                <li class="breadcrumb-item active" aria-current="page">Crear Nuevo</li>
              </ol>
            </nav>
          </div>
          <div class="card">
            <div class="card-body">
              {{-- <h4 class="card-title">Categorías</h4> --}}
             
              
              <div class="row">
                  <div class="col-12">
                 
                 <form action="{{ route('IngresoEmpleado.store') }}" method="POST" name="IngresoEmpleado"> 
                   @csrf 
                  @include('ingresoempleados.form') 
	                     <div class="form-group mr-2">
                          
							         </div>
				          </form>
                  	
                </div>
              </div>
            </div>
          </div>
        </div>
        @endsection
@section('scripts')
<script src="{{ asset('assets/js/select2.js')}}"></script>
<script src="{{ asset('assets/js/alerts.js')}}"></script>
<script src="{{ asset('assets/js/avgrund.js')}}"></script>
<script src="{{ asset('assets/js/toastr.min.js')}}"></script>



@endsection