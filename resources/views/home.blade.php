@extends('layouts.templates')
@section('title', 'PAGINA PRINCIPAL')
@section('styles')
  <style type="text/css">
    .unstyled-button{
      border: none;
      padding: 0;
      background: none;
    }

    .img-fluid{
        width:80%;
        height:70vh;
        animation: lights 10s linear infinite;
    }
    .img-fondo2{
        width:70wh;
        height:70vh;
        position:absolute;
     margin-left:15%;
     filter:grayscale(1);
     filter:opacity(0.1);
    }
@keyframes lights{
  0%{
    width:80%;
    filter: drop-shadow(0px 2px 10px rgba(255, 255, 0, 0.8));
  }
  25%{
    width:0%;
    filter: drop-shadow(10px 2px 10px rgba(0, 255, 0, 0.8));
  }
  50%{
    width:80%;
    filter: drop-shadow(0px 2px 10px rgba(255, 255, 0, 0.8));
  }
  51%{
    margin-top:0vh;
    height: 70vh;
    filter: drop-shadow(0px 2px 10px rgba(255, 255, 0, 0.8));
  }
  75%{
    margin-top:35vh;
    height: 0vh;
    filter: drop-shadow(0px 10px 10px rgba(0, 255, 255, 0.8));
  }
  100%{
    margin-top:0vh;
    height: 70vh;
    filter: drop-shadow(0px 2px 10px rgba(140, 255, 140, 0.8));
  }

}

.sidebar-offcanvas:hover{
  background:rgba(245,245,245,0.8);
}

.navbar-menu-wrapper{
  display:none;
}

  </style>
  
@endsection
@section('content')
<img class ="img-fondo2" src="{{asset('images/biloba.jpg')}}" width="50%" />
<center>
<!-- <img class ="img-fluid" src="{{asset('images/biloba.jpg')}}" width="50%" /> -->
</center>

@endsection

@section('scripts')

		
@endsection