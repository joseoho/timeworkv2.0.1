@extends('layouts.templates')
@section('title', 'HIstorico')
@section('styles')
  <style type="text/css">
    .unstyled-button{
      border: none;
      padding: 0;
      background: none;
    }
  </style>
@endsection
@section('content')


	<div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
            <img class ="img-fondo2" aling="left" src="{{asset('images/biloba.jpg')}}" width="10%" />
            Reportes de Ingresos por Empleados del dia:&nbsp;{{$date}}
            </h3>
            
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                
                <li class="breadcrumb-item active" aria-current="page">Historico</li>
              </ol>
            </nav>
          </div>
          <div class="card">
            <div class="card-body">
              <div class="d-flex justify-content-between">
                <div>
                  <h3>Empleado</h3>
                </div>
                <div>
                  <a href="javascript:window.print()" class="btn btn-success">
                    <i class="fas fa-download"></i>
                  </a>
                   {{-- <a href="{{ route('Empleado.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i></a > --}}
                </div>
              </div><br>

              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>Id</th>
                            <th>Cedula</th>
                            <th>Nombre</th>
                            <th>Entrada/salida</th>
                            <th>Diferencia</th>
                            
                            {{-- <th>Acciones</th> --}}
                        </tr>
                      </thead>
                      <tbody>
                          @foreach ($ingresos as $ingreso)
                          <tr>
                                <td scope="row">{{ $ingreso->id }}</td>
                                <td>{{ $ingreso->cinumber }}</td>
                                <td>{{ $ingreso->name }}</td> 
                                <td>{{ $ingreso->timestamp }}</td>
                                <td>{{ $diferencia}}</td>
                          </tr>
                          @endforeach
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
 @endsection
@section('scripts')
	<script src="{{asset ('assets/js/data-table.js')}}"></script>

  
@endsection 