@extends('layouts.templates')
@section('title', 'Registrar su Ingreso y/o Retiro')
@section('styles')
  <link rel="stylesheet" href="{{ asset('assets/css/toastr.min.css') }}">
@endsection
@section('content')
	<div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
              Control de Asistencia
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="">Registro de Ingreso</a></li>
                <li class="breadcrumb-item active" aria-current="page">Crear Nuevo</li>
              </ol>
            </nav>
          </div>
          <div class="card">
            <div class="card-body">
              {{-- <h4 class="card-title">Categorías</h4> --}}

              <div class="row">
                  <div class="col-12">
                 <form action="" method="POST" name="IngresoEmpleado"> 
                   @csrf 
                   @include('buscadordeimagen.form')

                   
	                     <div class="form-group mr-2">
                          
							         </div>
				          </form>
                  <input type="text" id="paraula">
                    <div id="contenidor"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endsection
@section('scripts')
<script src="{{ asset('assets/js/select2.js')}}"></script>
<script src="{{ asset('assets/js/alerts.js')}}"></script>
<script src="{{ asset('assets/js/avgrund.js')}}"></script>
<scrip src="{{ asset('assets/js/toastr.min.js')}}"></script>

<script>

const array_imatges = [
  "flor.jpg",
  "manta.jpg",
  "fleca.jpg",
  "ment.webp",
  "feli.jpg",
  "menta.jpg",
  "fletxa.jpg",
];

window.onload = function () {
    // Input donde se introduce el texto a buscar
    let palabra = document.getElementById("parula");
    // Ejecutar con evento input
    palabra.addEventListener("input", registrar);
}

function registrar(){
    let cont = document.getElementById('contenidor');
    // Limpiar contenedor
    cont.innerHTML = '';
    
    let texto = document.getElementById("parula").value;
    // Salir si está vacío
    if(texto.trim() == '') {
        return;
    }
    // Filtrar imágenes de acuerdo al texto
    let answer = array_imatges.filter(
        // Convertir nombre y valor a minúsculas para comparar
        item => item.toLowerCase().indexOf(texto.toLowerCase()) != -1
    ); 
    // Recorrer resultados
    answer.forEach(item => {
        // Crear imagen, definiendo ancho y alto
        let img = new Image(300, 200);
        img.src = "img/" + item;
        img.alt = item;
        img.style.display = 'inline-block';
        // Agregar a contenedor
        cont.appendChild(img);
    });
}

</script>


@endsection

