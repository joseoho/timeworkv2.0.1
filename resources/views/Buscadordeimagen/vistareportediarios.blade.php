@extends('layouts.templates')
@section('title', 'Generar Reporte')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<div class="page-header">
            <h3 class="page-title">
            Informe de Asistencias - Personal-Laboratorio Clinico BILOBA
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/home">Pagina Principal</a></li>
                <li class="breadcrumb-item"><a href="">Articulos</a></li>
                <li class="breadcrumb-item active" aria-current="page">Crear Nueva</li>
              </ol>
            </nav>
          </div>
<form action="{{route('ReporteIngresosDiarios.store')}}" method="POST">
@csrf
<div class="row">
    <div class="col-sm-8">
        <div class="row">    
             <div class="card-body">
            {{-- <label for="codigo"><strong>Vendedor</strong></label>
				            <select class="form-control" name="empleado_id" id="empleado_id" required>
				                @foreach ($empleados as $empleado)
				                <option value="{{$empleado->cinumber}}">{{$empleado->name}}</option>
								@endforeach
				            </select> --}}
            <div class="col-sm-5">
            <label for="codigo"><strong>Fecha Inicio</strong></label>
                <input class="form-control" type="date" value="{{old('fecha_ini')}}" name="fecha_ini" id="fecha_ini" required>      
            </div>
            <div class="col-sm-5">
            <label for="codigo"><strong>Fecha Fin</strong></label>
                <input class="form-control" type="date" value="{{old('fecha_fin')}}" name="fecha_fin" id="fecha_fin" required> 
            </div>  
            <div class="col-sm-3">
                <button type="submit" class="btn btn-primary">Consultar</button>
            </div>                      
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('assets/js/select2.js')}}"></script>
<script src="{{ asset('assets/js/alerts.js')}}"></script>
<script src="{{ asset('assets/js/avgrund.js')}}"></script>
<script src="{{ asset('assets/js/toastr.min.js')}}"></script>

@endsection
<script>
$("body").on("keydown", "input, select, textarea", function(e) {
  var self = $(this),
    form = self.parents("form:eq(0)"),
    focusable,
    next;
  
  // si presiono el enter
  if (e.keyCode == 13) {
    // busco el siguiente elemento
    focusable = form.find("input,a,select,button,textarea").filter(":visible");
    next = focusable.eq(focusable.index(this) + 1);
    
    // si existe siguiente elemento, hago foco
    if (next.length) {
      next.focus();
    } else {
      // si no existe otro elemento, hago submit
      // esto lo podrías quitar pero creo que puede
      // ser bastante útil
      form.submit();
    }
    return false;
  }
});
</script>