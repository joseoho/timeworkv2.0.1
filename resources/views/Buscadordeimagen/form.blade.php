
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
										@if (session()->has('notificacion'))
                        					<div class="alert alert-success" role="alert">Registrado</div>
                        					{{session('notificacion')}}
                    					@endif
											
										@if (session()->has('noexiste'))
                         					 <div class="alert alert-danger" role="alert">Cedula no Registrada, Verifique</div>
											  {{session('noexite')}}
                      					@endif
<div class="row">
	
		<div class="col-12 grid-margin">
			<div class="card">
				<div class="row">
				
					<div class="col-md-6">
						
						<div class="card-body">
							
								<!-- <div class="form-group"> -->
								<div class="form-group">
									<label for="empleado_id"><strong>Cedula</strong></label>
									<input type="text" name="empleado_id" id="empleado_id" class="form-control" placeholder="Ingrese numero cedula"  tabindex="0" autofocus required>
								</div>


			


								
										<form action="" method="POST" name="IngresoEmpleado">
											@csrf
											<button class="btn btn-success float-right mt-4 ml-2 " id="agregar" tabindex="1" >Registar</button> 
											{{-- <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print m-r-5"></i> Imprimir</a> --}}
										</form>	
										
								</div>
								
					</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@section('scripts')
	
	
	@endsection
	<script>
$("body").on("keydown", "input, select, textarea", function(e) {
  var self = $(this),
    form = self.parents("form:eq(0)"),
    focusable,
    next;
  
  // si presiono el enter
  if (e.keyCode == 13) {
    // busco el siguiente elemento
    focusable = form.find("input,a,select,button,textarea").filter(":visible");
    next = focusable.eq(focusable.index(this) + 1);
    
    // si existe siguiente elemento, hago foco
    if (next.length) {
      next.focus();
    } else {
      // si no existe otro elemento, hago submit
      // esto lo podrías quitar pero creo que puede
      // ser bastante útil
      form.submit();
    }
    return false;
  }
});

'use strict';

const video = document.getElementById('video');
const canvas = document.getElementById('canvas');
const snap = document.getElementById("snap");
const errorMsgElement = document.querySelector('span#errorMsg');

const constraints = {
  audio: true,
  video: {
    width: 1280, height: 720
  }
};

// Access webcam
async function init() {
  try {
    const stream = await navigator.mediaDevices.getUserMedia(constraints);
    handleSuccess(stream);
  } catch (e) {
    errorMsgElement.innerHTML = `navigator.getUserMedia error:${e.toString()}`;
  }
}

// Success
function handleSuccess(stream) {
  window.stream = stream;
  video.srcObject = stream;
}

// Load init
init();

// Draw image
var context = canvas.getContext('2d');
snap.addEventListener("click", function() {
        context.drawImage(video, 0, 0, 640, 480);
});



</script>