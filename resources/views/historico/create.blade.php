 @extends('layouts.templates')
@section('title', 'Historico')
@section('content') 
@section('styles')
  <link rel="stylesheet" href="{{ asset('assets/css/toastr.min.css') }}">
@endsection
	<div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
            <img class ="img-fondo2" aling="left" src="{{asset('images/biloba.jpg')}}" width="10%" />
              Consultar Historico por Empleado
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('Empleado.index') }}">Empleados</a></li>
                <li class="breadcrumb-item active" aria-current="page">Crear Nuevo</li>
              </ol>
            </nav>
          </div>
          <div class="card">
            <div class="card-body">
              {{-- <h4 class="card-title">Categorías</h4> --}}

              <div class="row">
                  <div class="col-12">
                 <form action="{{ route('Historico.store') }}" method="POST" name="empleados"> 
                    @csrf 
                        @include('historico.form')
	                     <div class="form-group mr-2">
                          <a href="/Historico" class="btn btn-light">Regresar</a>
                          <button type="submit" class="btn btn-primary">Consultar</button>
							        </div>
				         </form>
                  	
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection 
@section('scripts')
<script src="{{ asset('assets/js/select2.js')}}"></script>
@endsection