@extends('layouts.templates')
@section('title', 'HIstorico')
@section('styles')
  <style type="text/css">
    .unstyled-button{
      border: none;
      padding: 0;
      background: none;
    }
  </style>
@endsection
@section('content')


	<div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
            <img class ="img-fondo2" aling="left" src="{{asset('images/biloba.jpg')}}" width="10%" />
            Historico por Empleado 
            </h3>
            
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                
                <li class="breadcrumb-item active" aria-current="page">Historico</li>
              </ol>
            </nav>
          </div>
          <div class="card">
            <div class="card-body">
              <div class="d-flex justify-content-between">
                <div>
                  <h3>Empleado</h3>
                </div>
                <div>
                  <a href="javascript:window.print()" class="btn btn-success">
                    <i class="fas fa-download"></i>
                  </a>
                   {{-- <a href="{{ route('Empleado.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i></a > --}}
                </div>
              </div><br>

              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>ID</th>
                            <th>CEDULA</th>
                            <th>HORA</th>
                            <th>FOTO</th>
                            
                            {{-- <th>Acciones</th> --}}
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($ingresos as $ingreso)
                        	<tr>
                            <td scope="row">{{ $ingreso->id }}</td>
                            <td>{{ $ingreso->cinumber }}</td>
                            <!-- <td>{{ $ingreso->photourl }}</td> -->
                            <td>{{ $ingreso->timestamp }}</td>
                            {{-- <td><a href="{{route('Historico.show',  $ingreso->id)}}"><img src="{{$ingreso->photourl}}" width="50%" title="Detalle del Empleado" onmouseover="" onmouseout=""  /></a></td> --}}
                            <td><a href="{{route('Historico.show',  $ingreso->id)}}"><img src="{{asset($ingreso->photourl)}}" width="50%" title="Detalle del Empleado" onmouseover="" onmouseout=""  /></a></td> 
                            
                            <td>
                            {{-- <form action="{{route('Historico.destroy',$ingreso->id)}}" method="POST">    <a href="{{ route('Historico.edit', $ingreso) }}" title="Editar" class="jsgrid-button jsgrid-edit-button">
                            @method('DELETE')
                                            @csrf 
                              <a href="{{ route('Historico.edit', $ingreso) }}" title="Editar" class="jsgrid-button jsgrid-edit-button">
                                   <i class="far fa-edit"></i>
                                 </a>
                                 <button title="Eliminar" class="jsgrid-button jsgrid-delete-button unstyled-button">
                                   <i class="far fa-trash-alt" type="submit"></i>
                                 </button>
                              </form> --}}
                            </td>
                        </tr>
                        @endforeach
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
 @endsection
@section('scripts')
	<script src="{{asset ('assets/js/data-table.js')}}"></script>

  
@endsection 