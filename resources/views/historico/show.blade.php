@extends('layouts.templates')
@section('title', 'Actuliza Hora de Ingreso')
@section('content')
                                   
	<div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
            Actuliza Hora de Ingreso de Empleado
            <img src="{{asset($ingreso->photourl)}}" width="30%" title="foto" />
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/home">Incio</a></li>
                <li class="breadcrumb-item"><a href="{{ route('Historico.store') }}">Historico</a></li>
                <li class="breadcrumb-item active" aria-current="page">Consulta</li>
              </ol>
            </nav>
          </div>
          <div class="card">
            <div class="card-body">
              {{-- <h4 class="card-title">Categorías</h4> --}}

              <div class="row">
                  <div class="col-12">
                  @if($errors->any())

                  @endif
                  @if (session()->has('actualizada'))
                        					<div class="alert alert-success" role="alert">Fecha/Hora Actualizada Correctamente'</div>
                        					{{session('actualizada')}}
                    					@endif   
                  
                  <form action="/Historico/{{$ingreso->id}}" method="POST">    
                    @csrf
                    @method('PATCH')
	                     <div class="form-group">
			                  <label for="name"><strong>ID del Ingreso</strong></label>
			                  <input type="text" value="{{$ingreso->id}}" name="id" id="id" class="form-control" disabled>
			                </div>


			                <div class="form-group">
                      @if ($errors->has('hora'))
                        <span class="alert alert-danger">
                          <span>{{$errors->first('hora')}}</span>
                        </span>
	                    @endif

                      
                      <div class="form-group">
			                  <label for="hora"><strong>Hora</strong></label>
			                  <input type="text" name="hora" value="{{$ingreso->timestamp}}" id="hora" class="form-control" max="00:00:00" min="23:59:59" >
			                </div> 
	                    
                      <div class="form-group mr-2">
                       <a href="/Historico" class="btn btn-light">Regresar</a>
                       <button type="submit" class="btn btn-primary">Actualizar</button>
      							
      							   </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
<script src="/images/valida.js" async></script>