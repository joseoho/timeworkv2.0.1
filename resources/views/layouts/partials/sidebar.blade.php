<nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <div class="profile-image">
                
              </div>
              <div class="profile-name">
                <p class="name">
                 
                </p>
                <p class="designation">
                {{-- Saludos Amigo(a):<h3>{{ Auth::user()->name }}<h3> --}}
                </p>
              </div>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/home">
              <i class="fa fa-home menu-icon"></i>
              <span class="menu-title">Inicio</span>
            </a>
          </li>
          <li class="nav-item" id="menuempleado" >
            <a class="nav-link" data-toggle="collapse" href="#employees" aria-expanded="false" aria-controls="employees">
              <i class="fab fa-trello menu-icon"></i>
              <span class="menu-title">Empleados</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="employees"  >
              <ul class="nav flex-column sub-menu">
                <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="/Empleado">Registrar</a></li>
                {{-- <li class="nav-item"> <a class="nav-link" href="/pdfempleados">Reporte</a></li> --}}
              </ul>
            </div>
          </li>
          <li class="nav-item" id="">
            <a class="nav-link" data-toggle="collapse" href="#intros" aria-expanded="false" aria-controls="intros">
              <i class="fab fa-trello menu-icon"></i>
              <span class="menu-title" >Registrar Ingresos</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="intros">
              <ul class="nav flex-column sub-menu">
              {{-- <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="/IngresoEmpleado">Index</a></li> --}}
              <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="/IngresoEmpleado/create">Control de Asistencias</a></li>
              <li class="nav-item d-none d-lg-block" id="historico" > <a class="nav-link" href="/Historico">Historicos</a></li>
                
              </ul>
            </div>
          </li>
         {{-- <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#purchases" aria-expanded="false" aria-controls="purchases">
              <i class="fab fa-trello menu-icon"></i>
              <span class="menu-title">Compras</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="purchases">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="r">Proveedores</a></li>
                <li class="nav-item"> <a class="nav-link" href="">Registrar Compras</a></li>
                <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="">Ver Comprar</a></li>
                </ul>
            </div>
          </li>

          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#money" aria-expanded="false" aria-controls="sales">
              <i class="fab fa-trello menu-icon"></i>
              <span class="menu-title">Moneda</span>
              <i class="menu-arrow"></i>
            </a>
              <div class="collapse" id="money">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="">Tasas</a></li>
                  <li class="nav-item"> <a class="nav-link" href="">Actualizar Precios</a></li>
                </ul>
              </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#sales" aria-expanded="false" aria-controls="sales">
              <i class="fab fa-trello menu-icon"></i>
              <span class="menu-title">Pedidos</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="sales">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="">Vendedores</a></li>
                <li class="nav-item"> <a class="nav-link" href="/Pedido">Pedidos</a></li>
              </ul>
            </div>
          </li>
          --}}   
          <li class="nav-item" id="reportes" >
            <a class="nav-link" data-toggle="collapse" href="#reports" aria-expanded="false" aria-controls="reports">
              <i class="fab fa-trello menu-icon"></i>
              <span class="menu-title">Reportes</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="reports" >
              <ul class="nav flex-column sub-menu">
              {{-- <li class="nav-item"> <a class="nav-link" href="/ReporteIngresosxEmpleado">Reportes</a></li> --}}
                <li class="nav-item"> <a class="nav-link" href="/ReporteIngresosDiarios">Reportes Ingresos Diarios</a></li>
              </ul>
            </div>
          </li>
       
        </ul>
        
      </nav>