			@if($errors->any())
			@endif
					@if (session()->has('duplicado'))
                      <div class="alert alert-warning" role="alert">Empleado ya se encuentra Registrado</div>
                      {{session('duplicado')}}
					@endif   

@if ($errors->has('cedula'))
    <span class="alert alert-warning">
    <span>{{$errors->first('cedula')}}</span>
    </span>
@endif
<div class="form-group">
	<label for="cedula"><strong>Cedula</strong></label>
	<input type="text" name="cedula" id="cedula" class="form-control" placeholder="Ingrese una cedula">
</div>
@if ($errors->has('nombre'))
    <span class="alert alert-warning">
    <span>{{$errors->first('nombre')}}</span>
    </span>
@endif
<div class="form-group">
	<label for="nombre"><strong>Nombre</strong></label>
	<input type="text" name="nombre" id="nombre" class="form-control" placeholder="Ingrese el nombre" autofocus >
</div>
@if ($errors->has('role'))
    <span class="alert alert-warning">
    <span>{{$errors->first('role')}}</span>
    </span>
@endif
<div class="form-group">
	<label for="role"><strong>ROLE</strong></label>
	<input type="text" name="role" id="role" class="form-control" placeholder="Ingrese una role" >
</div>
@if ($errors->has('nota'))
    <span class="alert alert-warning">
    <span>{{$errors->first('nota')}}</span>
    </span>
@endif
<div class="form-group">
	<label for="nota"><strong>Nota</strong></label>
	<input type="text" name="nota" id="nota" class="form-control" placeholder="Ingrese una nota" >
</div>
<div class="form-group">
		<label><strong>Status</strong></label>
		<select id="status" name="status" class="form-control text-center">
		<option value="Elegir">-- Selecione --</option>
		<option value="ACTIVO">ACTIVO</option>
		<option value="VACACIONES">VACACIONES</option>
		<option value="REPOSO">REPOSO</option>
		</select>
	</div>
