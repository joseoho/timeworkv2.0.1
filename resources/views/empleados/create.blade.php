 @extends('layouts.templates')
@section('title', 'Registrar Cliente')
@section('content') 
	<div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
              Crear Empleado
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('Empleado.index') }}">Empleados</a></li>
                <li class="breadcrumb-item active" aria-current="page">Crear Nuevo</li>
              </ol>
            </nav>
          </div>
          <div class="card">
            <div class="card-body">
              {{-- <h4 class="card-title">Categorías</h4> --}}

              <div class="row">
                  <div class="col-12">
                    
                 <form action="{{ route('Empleado.store') }}" method="POST" name="empleados"> 
                    @csrf 
                        @include('Empleados.form')
	                     <div class="form-group mr-2">
                          <a href="/Empleado" class="btn btn-light">Regresar</a>
                          <button type="submit" class="btn btn-primary">Guardar</button>
							        </div>
				         </form>
                  	
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection 