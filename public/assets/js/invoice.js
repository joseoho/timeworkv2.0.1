$(document).on('ready',funcMain);
let i=1;
function funcMain() 
{
    $("#add_row").on('click',newRowTable);
    
    $("body").on('click',".fa-eraser",deleteProduct);
	$("body").on('click',".fa-edit",editProduct);
	
}

var inputForm2 = document.getElementById("id_articulo");

inputForm2.addEventListener("keyup", function (event) {
	if (event.keyCode === 13) {
		newRowTable();
	}
}

)

var inputForm = document.getElementById("iva");
inputForm.addEventListener("keyup", function (event) {
	if (event.keyCode === 13) {
		newRowTable();
	}
}

)

function deleteProduct(){

	var _this = this;
	var array_fila=getRowSelected(_this);
	calculateTotals(array_fila[1],array_fila[2],array_fila[3],array_fila[4],array_fila[5],2)

	$(this).parent().parent().fadeOut("slow",function(){$(this).remove();});
}

function editProduct(){


	var _this = this;
	var array_fila=getRowSelected(_this);
    document.getElementById("id_articulo").value = array_fila[0];
	document.getElementById("cantidad").value = array_fila[1];
    document.getElementById("precio_compra").value = array_fila[2];
    document.getElementById("precio_venta").value = array_fila[3];
    document.getElementById("iva").value = array_fila[6];
  

	calculateTotals(array_fila[1],array_fila[2],array_fila[3],array_fila[4],array_fila[5],2)

	$(this).parent().parent().fadeOut("slow",function(){$(this).remove();});
}

function getRowSelected(objectPressed){
	
	var a=objectPressed.parentNode.parentNode;
	
	var descripcion=a.getElementsByTagName("td")[1].getElementsByTagName("p")[0].innerHTML;
	var cantidad=a.getElementsByTagName("td")[2].getElementsByTagName("p")[0].innerHTML;
	var preciocompra=a.getElementsByTagName("td")[3].getElementsByTagName("p")[0].innerHTML;
	var precioventa=a.getElementsByTagName("td")[4].getElementsByTagName("p")[0].innerHTML;
	var subtotal=a.getElementsByTagName("td")[5].getElementsByTagName("p")[0].innerHTML;
	var total=a.getElementsByTagName("td")[6].getElementsByTagName("p")[0].innerHTML;
	var iva=a.getElementsByTagName("td")[8].getElementsByTagName("p")[0].innerHTML;
	
	var array_fila = [descripcion, cantidad, preciocompra, precioventa, subtotal, total, iva];
	
	return array_fila;
	
}

function newRowTable()
{
	var precioventa=document.getElementById("precio_venta").value;
	var preciocompra=document.getElementById("precio_compra").value;
	var descripcion=document.getElementById("id_articulo").value;
	var cantidad=document.getElementById("cantidad").value;
	var iva=parseFloat(document.getElementById("iva").value);
	var subtotal=parseFloat(cantidad)*parseFloat(preciocompra);
	var impuesto=parseFloat(subtotal)*parseFloat(iva);
	var total_n=parseFloat(subtotal)+parseFloat(impuesto);

	var name_table=document.getElementById("tabla_factura");

    var row = name_table.insertRow(0+1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    var cell6 = row.insertCell(5);
    var cell7 = row.insertCell(6);
	var cell8 = row.insertCell(7);
    var cell9 = row.insertCell(8);
	
	cell1.innerHTML = i++;
	cell2.innerHTML = '<p name="descripcion_p[]" class="non-margin">'+descripcion+'</p>';
	cell3.innerHTML = '<p name="cantidad_p[]" class="non-margin">'+cantidad+'</p>';
    cell4.innerHTML = '<p name="preciocompra_p[]" class="non-margin">'+preciocompra+'</p>';
    cell5.innerHTML = '<p name="precioventa_p[]" class="non-margin">'+precioventa+'</p>';
    cell6.innerHTML = '<p name="subtotal_p[]" class="non-margin">'+subtotal+'</p>';
	cell7.innerHTML = '<p name="total_p[]" class="non-margin">'+total_n+'</p>';
    cell8.innerHTML = '<span class="icon fa-edit"></span><span class="icon fa-eraser"></span>';
    cell9.innerHTML = '<p name="iva_p[]" class="non-margin" style="position:absolute; filter:opacity(0);">'+iva+'</p>';

	calculateTotals(cantidad, preciocompra, precioventa, subtotal, total_n, 1);

	document.getElementById("cantidad").value="";
	document.getElementById("precio_venta").value="";
	document.getElementById("precio_compra").value="";
	document.getElementById("producto").value="";
	document.getElementById("iva").value="";

	document.getElementById("precio_compra").focus();
}


function calculateTotals(cantidad, preciocompra, precioventa, subtotal, totaln, accion) {    

	var t_cantidad=parseFloat(document.getElementById("total_cantidad").innerHTML);
    var t_precioventa=parseFloat(document.getElementById("total_precioventa").innerHTML);
	var t_preciocompra=parseFloat(document.getElementById("total_preciocompra").innerHTML);
    var t_subtotal=parseFloat(document.getElementById("total_subtotales").innerHTML);
    var t_total=parseFloat(document.getElementById("total_total").innerHTML);

if(accion==1){

    document.getElementById("total_cantidad").innerHTML=parseFloat(t_cantidad)+parseFloat(cantidad);
	document.getElementById("total_precioventa").innerHTML=parseFloat(t_precioventa)+parseFloat(precioventa);
	document.getElementById("total_preciocompra").innerHTML=parseFloat(t_preciocompra)+parseFloat(preciocompra);
	document.getElementById("total_subtotales").innerHTML=parseFloat(t_subtotal)+parseFloat(subtotal);
	document.getElementById("total_total").innerHTML=parseFloat(t_total)+parseFloat(totaln);

}else if(accion==2){


    document.getElementById("total_cantidad").innerHTML=parseFloat(t_cantidad)-parseFloat(cantidad);
	document.getElementById("total_precioventa").innerHTML=parseFloat(t_precioventa)-parseFloat(precioventa);
	document.getElementById("total_preciocompra").innerHTML=parseFloat(t_preciocompra)-parseFloat(preciocompra);
	document.getElementById("total_subtotales").innerHTML=parseFloat(t_subtotal)-parseFloat(subtotal);
	document.getElementById("total_total").innerHTML=parseFloat(t_total)-parseFloat(totaln);

}else{
    alert('Accion Invalida');
	alert(accion);
}

}

function format(input)
{
	var num = input.value.replace(/\,/g,'');
	if(!isNaN(num)){
		input.value = num;
	}
	else{/* alert('Solo se permiten numeros');*/
		input.value = input.value.replace(/[^\d\.]*/g,'');
	}
}

