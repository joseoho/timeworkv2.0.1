<?php

namespace App\Http\Controllers\Historico;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\HistoricoRequest;
use Carbon\Carbon;
use App\employee;
use App\Timemark;
class HistoricoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        set_time_limit(300);
        $empleados = employee::get();
        return view('historico.create', compact('empleados'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        set_time_limit(300);
        $empleados=employee::all();
        $cedula = $request->empleado_id;
        $fi = $request->fecha_ini.' 00:00:00';
        $ff = $request->fecha_fin.' 23:59:59';
    
        //$compras = DB::table('compras')->orderBy('FECHA', 'asc')->where('PROVEEDOR_ID', [$id])->get();
        //$compras = DB::table('compras')->orderBy('FECHA', 'asc')->whereBetween('FECHA', [$fi, $ff])->get();
            $ingresos = DB::table('timemark')->orderBy('timestamp', 'asc')->where('cinumber', [$cedula])->get()->whereBetween('timestamp', [$fi, $ff]);
           if ($ingresos->count())
            {
                return view ('historico.index', compact('ingresos'));
                
            }else{
                return back()->with('noexiste', '');
            }
            }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ingreso=Timemark::findOrFail($id);
        return view('historico.show')->with('ingreso',$ingreso);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ingreso=Timemark::findOrFail($id);
        //$articulos=Articulo::all();
         return view('historico.edit')->with('ingreso',$ingreso);      }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HistoricoRequest $request, $id)
    {
           
        $fi = date('Y-m-d 00:00:00');
        $ff = date('Y-m-d 23:59:59');
        $hora=Timemark::findOrFail($id);
        $hora->timestamp=$request->input('hora');
        $hora->save();
            if($hora){
                return redirect('/Historico/'.$id)->with('actualizada','');
            }
            return redirect('/Historico/'.$id)->with('invalid','');
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ingreso=Timemark::find($id);
        $ingreso->delete();
        return redirect()->route('historico.index'); 
    }
}
