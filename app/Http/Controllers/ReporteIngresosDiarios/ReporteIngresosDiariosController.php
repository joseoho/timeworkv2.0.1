<?php

namespace App\Http\Controllers\ReporteIngresosDiarios;



use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\employee;
use App\Timemark;
use Carbon\Carbon;
use App\LlamaEmpleado;
//pruebas de actualizacion en ambos repos
//nuevamente

class ReporteIngresosDiariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleados=employee::all();
       
        return view ('ingresoempleados.vistareportediarios', compact('empleados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $fechainicial = new DateTime($fecha1);
        // //fecha inicial 
        // $fechaactual = new DateTime($fecha2);
        // //fecha de cierre 
        // $diferencia = $fechainicial->diff($fechaactual);
        set_time_limit(300);
        $date=Carbon::now('America/Caracas');
        $fi = new Carbon($request->fecha_ini.' 00:00:00');
        $ff = new Carbon($request->fecha_fin.' 23:59:59');
               $ingresos = timemark::join('employee as e','e.cinumber','=','timemark.cinumber')
               ->select('timemark.id','timemark.cinumber','timemark.timestamp','e.name')
               ->orderBy('timemark.cinumber', 'asc')->whereBetween('timemark.timestamp', [$fi, $ff])
               ->get();
               $diferencia = $fi->diff($ff)->format('%I:%S');;
                    if ($ingresos->count()) {
                                return view ('reportes.reporteingresosdiariosxempleado',compact('ingresos','date','diferencia'));
                        //    $pdf =\PDF::loadView('reportes.reporteingresosdiariosxempleado', compact('ingresos','fi'));
                        //    return $pdf->stream();
                    }else{
                        return back()->with('noexiste', '');
                        
                    }
                                        

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
