<?php

namespace App\Http\Controllers\ReportexEmpleado;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\employee;
use App\Timemark;

class ReportexEmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleados=employee::all();
        return view ('ingresoempleados.vistareporte', compact('empleados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        set_time_limit(300);
        $empleados=employee::all();
        
        $fi = $request->fecha_ini.' 00:00:00';
        $ff = $request->fecha_fin.' 23:59:59';
        $cedula = $request->empleado_id;
    
        //$compras = DB::table('compras')->orderBy('FECHA', 'asc')->where('PROVEEDOR_ID', [$id])->get();
        //$compras = DB::table('compras')->orderBy('FECHA', 'asc')->whereBetween('FECHA', [$fi, $ff])->get();
            $ingresos = DB::table('timemark')->orderBy('timestamp', 'asc')->where('cinumber', [$cedula])->get()->whereBetween('timestamp', [$fi, $ff]);
           if ($ingresos->count())
            {
                $pdf =\PDF::loadView('reportes.reporteingresosxempleado', compact('ingresos','empleados'));
            return $pdf->stream();
                
            }else{
                return back()->with('noexiste', '');
            }
        

        
        //return view('prueba.pruebaa', compact('proveedores','compras'));
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
