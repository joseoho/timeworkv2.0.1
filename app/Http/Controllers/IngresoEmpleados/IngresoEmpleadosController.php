<?php

namespace App\Http\Controllers\IngresoEmpleados;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Http\Requests\Purchase\StoreRequest;
use App\Http\Requests\Purchase\UpdateRequest;
use App\Http\Controllers\Controller;
//use Intervention\Image\ImageManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Image;
use Intervention\Image\Exception\NotReadableException;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Timemark;
use App\employee;

class IngresoEmpleadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   

    public function index()
    {
        //probando el repo
        $empleados = employee::get();
        return view('ingresoempleados.formhistorico', compact('empleados'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //se modifico este linea 46 y 49
        $fecha=Carbon::now('America/Caracas');
        //$fecha = $fecha->format('Y-m-d 00:00:00');
        $empleados = employee::get();
        return view('ingresoempleados.create', compact('empleados','fecha'));  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
     {

    
//phpinfo();

//'/timework/uploads/'
  $baseDIR = $_SERVER['DOCUMENT_ROOT']. '/timework/uploads/'.date("m", time())."/"; //C:\laragon\www\BILOBA\public  
  $baseURL = 'http://' . $_SERVER['HTTP_HOST'].'/timework/uploads/' .date("m", time())."/"; //127.0.0.1:8000/images/01/
  
  if (!file_exists($baseDIR)) {
    mkdir($baseDIR, 0755, true);
  }


             $cedula=$request->empleado_id;
             $img    = $request->get('data_url');
             $img    = str_replace('data:image/png;base64,', '', $img);
             $img    = str_replace(' ', '+', $img);
             $decodifica = base64_decode($img,true);
             $extension  = "png";
             $filename = Str::random(10).$cedula .'.'.$extension;
             $path    = $baseDIR;
             $success = file_put_contents($baseDIR .$filename,$decodifica);
             $imageURL = $baseURL . $filename;
                            if (employee::where('cinumber', $cedula)->exists()) {
                                $seregistraempleado =   new Timemark();
                                $seregistraempleado->cinumber = $request->input('empleado_id'); //bueno
                                $seregistraempleado->photourl = ltrim($imageURL,'.');//bueno
                                //dd($seregistraempleado);
                                $seregistraempleado->save();//bueno
                                return back()->with('notification', 'Registrado');//bueno
                             }
                            
                                 return back()->with('noexiste', 'cedula no registrada, verifique');//buenoi
                            
                                            
                           
         
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

  
    public function show($id)
    {
      

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
