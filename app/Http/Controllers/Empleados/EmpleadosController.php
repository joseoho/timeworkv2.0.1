<?php

namespace App\Http\Controllers\Empleados;

use App\Http\Requests\Purchase\StoreRequest;
use App\Http\Requests\Purchase\UpdateRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\EmployeeRequest;
use Illuminate\Support\Facades\DB;
//use GuzzleHttp\Client;
use Http;
use App\employee;

class EmpleadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        set_time_limit(300);
        $empleados = employee::get();
        return view('Empleados.index', compact('empleados'));  
        //return $empleados;
    
            // $empleados = Http::get('https://newsdata.io/api/1/news?apikey=pub_798210afba7ae750836122bdb42e43b0a1a8');
            // $mostrarempleados   =   $empleados->json();
            // return view('empleados.index', compact('mostrarempleados'));
            //return $mostrarempleados;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Empleados.create');    
      }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {

        $cedula=$request->cedula;
        if (employee::where('cinumber', $cedula)->exists()) {
            return back()->with('duplicado', '');
        }
        employee::create([
            // 'CODIGO'=>Request('codigo'),
                'cinumber'=>Request('cedula'), 
                'name'=>Request('nombre'),
                'role' =>Request('role'),
                'note' =>Request('note'),
                'status' =>Request('status'),
             
         ]);
         return redirect('/Empleado');    
        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empleado=employee::findOrFail($id);
        //$articulos=Articulo::all();
         return view('Empleados.edit')->with('empleado',$empleado);   
         }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $empleado=employee::findOrFail($id);
        $empleado->cinumber=$request->input('cedula');
        $empleado->name=$request->input('nombre');
        $empleado->role=$request->input('role');
        $empleado->note=$request->input('nota');
        $empleado->status=$request->input('status');
        $empleado->fill($request->all());
        $empleado->save();
          return redirect('/Empleado');    

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empleado=employee::find($id);
        $empleado->delete();
        return redirect()->route('Empleado.index');    }
}
