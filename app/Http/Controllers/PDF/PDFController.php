<?php

namespace App\Http\Controllers\PDF;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\employee;
use App\Timemark;
class PDFController extends Controller
{
    public function PDF(){

        $empleados=employee::paginate(20);

        $pdf =\PDF::loadView('reportes.showempleados', compact('empleados'));
        //return $pdf->download('empleados.pdf');
        return $pdf->stream();
        //return view ('articulos.showarticulo')->with('articulos',$articulos);
   }

   public function PDFIngresos(){

    $empleados=Timemark::paginate(150);
    //$empleados=Timemark::all();
    $pdf =\PDF::loadView('reportes.showingresosempleados', compact('empleados'));
    //return $pdf->download('empleados.pdf');
    return $pdf->stream();
    //return view ('articulos.showarticulo')->with('articulos',$articulos);
}

}
