<?php

namespace App;
//use App\employee;
//use App\Timemark;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class employee extends Model
{
    Protected $primaryKey = "id";
    protected $table ='employee';
    protected $fillable = [
        
        'cinumber',
        'name',
        'role',
        'note',
        'status',
    ];

    public function Entradas()//Articulo_Categoria
    {
        //return $this->belongsTo(Categoria::Class);
        return $this->belongsTo('App\Timemark','cinumber','cinumber');
    } 
    
}
