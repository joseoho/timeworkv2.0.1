<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Empleados\EmpleadosController;
use App\Http\Controllers\Historicos\HistoricosController;
use App\Http\Controllers\IngresoEmpleado\IngresoEmpleadosController;
use App\Http\Controllers\PDF\PDFController;
use App\Http\Controllers\ReporteIngresosDiarios\ReporteIngresosDiariosController;
use App\Http\Controllers\ReportexEmpleado\ReportexEmpleadoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth::routes();
Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('Empleado','Empleados\EmpleadosController');
Route::resource('IngresoEmpleado','IngresoEmpleados\IngresoEmpleadosController');
Route::resource('Historico','Historico\HistoricoController'); //esta es la ruta nueva
Route::get('pdfempleados','PDF\PDFController@PDF')->name('DESCARGARPDF');
Route::get('pdfingresosempleados','PDF\PDFController@PDFIngresos')->name('DESCARGARPDFINGRESOS');
Route::resource('ReporteIngresosxEmpleado','ReportexEmpleado\ReportexEmpleadoController');
Route::resource('ReporteIngresosDiarios','ReporteIngresosDiarios\ReporteIngresosDiariosController');
//definicion de rutas laravel 8
// Route::get('/home', [HomeController::class, 'index'])->name('home');
// //Route::get('/user', [UserController::class, 'index']);
// Route::resource('/Empleado', [Empleados\EmpleadosController::class, 'index']);
// Route::resource('/IngresoEmpleado', [IngresoEmpleados\IngresoEmpleadosController::class, 'index']);
// //reportes
// Route::get('pdfempleados',[PDF\PDFController::class,"PDF"])->name('DESCARGARPDF');
// Route::get('pdfingresosempleados',[PDF\PDFController::class,'PDFIngresos'])->name('DESCARGARPDFINGRESOS');
// Route::resource('ReporteIngresosxEmpleado',[ReportexEmpleado\ReportexEmpleadoController::class,'index']);
// Route::resource('ReporteIngresosDiarios',[ReporteIngresosDiarios\ReporteIngresosDiariosController::class,'index']);





