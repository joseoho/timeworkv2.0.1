<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Timemark extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timemark', function (Blueprint $table) {
            $table->id();
            $table->string('cinumber');
            $table->string('photourl');
            $table->timestamps();
            
            $table->foreing('cinumber')->references('cinumber')->on('employee');


        });    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
